// Liste dependencies
const {src, dest, watch,series} = require('gulp')
const terser = require('gulp-terser')
const postcss = require('gulp-postcss')
const cleancss = require('gulp-clean-css')
const autoprefix = require('gulp-autoprefixer')

// Create task

// HTML
function copyHtml() {
    return src('src/*.html').pipe(dest('dist'))
}

// CSS
function cleanCss() {
    return src('src/tailwind.css')
        .pipe(postcss())
        .pipe(autoprefix())
        .pipe(cleancss())
        .pipe(dest('dist'))
}

// JS
function jsMin() {
    return src('src/*.js')
        .pipe(terser())
        .pipe(dest('dist'))
}

// Create watchtask
function watchTask() {
    watch('src/*.html', copyHtml)
    watch('src/*.js', jsMin)
    watch('src/tailwind.css', cleanCss)
}

// Default gulp

exports.default = series(
    copyHtml,
    jsMin,
    cleanCss,
    watchTask
)