// ********** START MENU HAMBURGER **********

// Select classes
const menuToggle = document.querySelectorAll('.toggle-menu')
const iconToggle = document.querySelector('.icone-toggle')

// Toggle class hidden
const toggleMenu = () => menuToggle.forEach(el => el.classList.toggle('hidden'))

// Add dinamic to icon menu hamburger
iconToggle.addEventListener('click', toggleMenu)

// ********** END MENU HAMBURGER **********

// ********** START GAME **********

const btnNew = document.querySelector('.btn-new')
const btnRoll = document.querySelector('.btn-roll')
const btnHold = document.querySelector('.btn-hold')

let scores, roundScore, activePlayer, playing

init()

// Next player
function nextPlayer() {
    // Update the roundScore if the rolled number 1
    roundScore = 0

    // Check the player
    activePlayer === 0 ? (activePlayer = 1) : (activePlayer = 0)

    document.getElementById('dice').style.display = 'none'

    // Update score current
    document.getElementById('current-0').textContent = 0
    document.getElementById('current-1').textContent = 0

    // Toggle active class
    document.querySelector('.panel-0').classList.toggle('active')
    document.querySelector('.panel-1').classList.toggle('active')

    // Toggle active-bg class
    document.querySelector('.player-0').classList.toggle('active-bg')
    document.querySelector('.player-1').classList.toggle('active-bg')
}

// ROLL BUTTON
btnRoll.addEventListener('click', () => {
    if(playing) {
        // Add bande the son
        const audio = new Audio('./dice.mp3')
        audio.play()
        audio.addEventListener('ended', () => {
            // Random number
            const resultat = Math.trunc(Math.random() * 6) + 1

            // Display the result
            const image = document.querySelector('#dice')
            image.style.display = 'block'
            image.src = `./images/de-${resultat}.png`

            // Update the roundScore if the rolled number NOT a 1
            if(resultat !== 1) {
                // Add score
                roundScore += resultat
                document.getElementById(`current-${activePlayer}`).textContent = roundScore
            } else {
                // Next player
                nextPlayer()
            }
        })
    }
})

// HOLD - add roundScore to scores
btnHold.addEventListener('click', () => {
    if(playing) {
        // Add current score to global score
        scores[activePlayer] += roundScore
        document.querySelector(`#score-${activePlayer}`).textContent = scores[activePlayer]

        // Check if player the winner
        if(scores[activePlayer] >= 100) {
            document.getElementById(`name-${activePlayer}`).textContent = 'Winner !!!'
            document.querySelector('#dice').style.display = 'none'

            // Remove classes 
            document.querySelector(`.panel-${activePlayer}`).classList.remove('active')
            document.querySelector(`.player-${activePlayer}`).classList.remove('active-bg')

            playing = false
        } else {
            // Next player
            nextPlayer()
        }
    }
})

// INIT

function init() {
    // Reseting score
    scores = [0, 0]
    roundScore = 0
    activePlayer = 0
    playing = true

    document.getElementById('dice').style.display = 'none'

    // Reseting all scores
    document.getElementById('score-0').textContent = 0
    document.getElementById('score-1').textContent = 0
    document.getElementById('current-0').textContent = 0
    document.getElementById('current-1').textContent = 0

    // Reseting players names
    document.getElementById('name-0').textContent = "Player 1"
    document.getElementById('name-1').textContent = "Player 2"

    // Removing classes from panels
    document.querySelector('.panel-0').classList.remove('active')
    document.querySelector('.panel-1').classList.remove('active')
    document.querySelector('.player-0').classList.remove('active-bg')
    document.querySelector('.player-1').classList.remove('active-bg')

    // Add classes active and active-bg
    document.querySelector('.panel-0').classList.add('active')
    document.querySelector('.player-0').classList.add('active-bg')
}

// BTN NEW GAME
btnNew.addEventListener('click', init)

// ********** END GAME **********